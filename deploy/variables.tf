variable "prefix" {
  default = "uaad"
}

variable "project" {
  default = "udemy-app-api-devops"
}

variable "contact" {
  default = "me"
}

variable "db_username" {
  description = "username for our RDS postgres instance"
}

variable "db_password" {
  description = "password for our RDS postgres instance"
}

variable "bastion_key_name" {
  default = "udemy-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "868699165885.dkr.ecr.us-east-1.amazonaws.com/udemy-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "868699165885.dkr.ecr.us-east-1.amazonaws.com/udemy-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}